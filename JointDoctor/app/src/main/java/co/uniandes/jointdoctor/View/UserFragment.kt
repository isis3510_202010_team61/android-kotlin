package co.uniandes.jointdoctor.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.View.user.UserListener
import co.uniandes.jointdoctor.databinding.UserFragmentBinding
import co.uniandes.jointdoctor.util.NetworkMonitorUtil
import co.uniandes.jointdoctor.viewmodel.AppViewModel
import co.uniandes.jointdoctor.viewmodel.UserViewModel
import co.uniandes.jointdoctor.viewmodel.UserViewModelFactory
import kotlinx.android.synthetic.main.user_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class UserFragment() : Fragment() , KodeinAware, UserListener {

    override val kodein by kodein()
    private val factory: UserViewModelFactory by instance()
    companion object {
        fun newInstance() = UserFragment()
    }
    private lateinit var viewModel: UserViewModel
    private lateinit var binding : UserFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this,factory).get(UserViewModel::class.java)
        viewModel.userListener= this
        binding = DataBindingUtil.inflate(inflater, R.layout.user_fragment, container, false)
        binding.userViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        viewModel.update()
    }


    override fun onStarted() {
        progressBar3.visibility = View.VISIBLE
    }

    override fun onSuccess(
        name: String,
        lastName: String,
        idNumber: Int,
        age: Int,
        email: String,
        gender: String,
        urlPhoto: String

    ) {
        progressBar3.visibility = View.GONE
        viewModel.update2(name, lastName,idNumber,age, email, gender, urlPhoto)
    }

}


