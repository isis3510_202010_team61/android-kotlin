package co.uniandes.jointdoctor

import android.app.Application
import co.uniandes.jointdoctor.Model.User
import co.uniandes.jointdoctor.View.UserFragment
import co.uniandes.jointdoctor.data.firebase.FirebaseSource
import co.uniandes.jointdoctor.viewmodel.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.*

class FirebaseApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@FirebaseApplication))

        bind() from singleton { FirebaseSource() }
        bind() from singleton { User(instance()) }
        bind() from provider { AuthViewModelFactory(instance()) }
        bind() from provider{ AppViewModelFactory(instance()) }
        bind() from provider { UserViewModelFactory(instance()) }
        bind() from provider { ContactsViewModelFactory(instance()) }


    }
}