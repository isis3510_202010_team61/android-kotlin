package co.uniandes.jointdoctor.util

import android.content.Context
import android.content.Intent
import co.uniandes.jointdoctor.View.AppActivity
import co.uniandes.jointdoctor.View.LoginActivity

fun Context.startHomeActivity() =
    Intent(this, AppActivity::class.java).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }

fun Context.startLoginActivity() =
    Intent(this, LoginActivity::class.java).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }