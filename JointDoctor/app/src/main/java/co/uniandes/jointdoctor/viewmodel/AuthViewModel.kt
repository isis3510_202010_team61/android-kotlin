package co.uniandes.jointdoctor.viewmodel

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.uniandes.jointdoctor.Model.User
import co.uniandes.jointdoctor.View.auth.AuthListener
import co.uniandes.jointdoctor.View.auth.SingUpListener
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.coroutines.launch


class AuthViewModel(private val repository: User): ViewModel() {

    val successSingUp = MutableLiveData<Boolean>(false)
    val password = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val emailError = MutableLiveData<String>()
    val nameSingUp = MutableLiveData<String>()
    val nameSingUpError = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val lastNameError = MutableLiveData<String>()
    val idNumber= MutableLiveData<String>()
    val idNumberError= MutableLiveData<String>()
    val age = MutableLiveData<String>()
    val ageError = MutableLiveData<String>()
    val password2 = MutableLiveData<String>()
    val passwordError2 = MutableLiveData<String>()
    val email2 = MutableLiveData<String>()
    val emailError2 = MutableLiveData<String>()
    var authListener: AuthListener? = null
    var singUpListener: SingUpListener? = null
    val male = MutableLiveData<Boolean>(true)
    val female = MutableLiveData<Boolean>(false)
    val other = MutableLiveData<Boolean>(false)
    val urlPhoto = MutableLiveData<String>()


    val user by lazy {
        repository.currentUser()
    }
    fun login(){
        //authentication started
        var termino = true
        if (email.value.isNullOrEmpty()) {
            emailError.value = "Por favor escribir un email"
            termino = false
        }
        else{
            if(!Patterns.EMAIL_ADDRESS.matcher(email.value).matches()){
                emailError.value = "Escribe un email válido"
                termino = false
            }
            else
                emailError.value = null
        }
        if( password.value.isNullOrEmpty())
        {
            passwordError.value = "Por favor escribir una contraseña"
            termino = false
        }
        else{
            if(!password.value.isNullOrEmpty() && password.value.toString().length < 6){
                passwordError.value = "La contraseña debe tener mas de 6 caracteres"
                termino = false
            }
            else
                passwordError.value = null
        }

        if (termino){
            authListener?.onStarted()
            viewModelScope.launch {
                val data = repository.login(email.value!!, password.value!!, authListener)
                if(data!=null)
                {
                    authListener?.onSuccess()
                }
            }
//            repository.login(email.value!!, password.value!!, authListener)
        }

    }
    fun singup(){
        var termino = true
        var gender = ""
        if (email2.value.isNullOrEmpty()) {
            emailError2.value = "Por favor escribir un email"
            termino = false
        }
        else{
            if(!Patterns.EMAIL_ADDRESS.matcher(email2.value).matches()){
                emailError2.value = "Escribe un email válido"
                termino = false
            }
            else
                emailError2.value = null
        }
        if( password2.value.isNullOrEmpty())
        {
            passwordError2.value = "Por favor escribir una contraseña"
            termino = false
        }
        else{
            if(password2.value.toString().length < 6){
                passwordError2.value = "La contraseña debe tener mas de 6 caracteres"
                termino = false
            }
            else
                passwordError2.value = null
        }
        if(nameSingUp.value.isNullOrEmpty())
        {
            nameSingUpError.value="You must write a name"
            termino = false
        }
        else
            nameSingUpError.value=null

        if(lastName.value.isNullOrEmpty())
        {
            lastNameError.value="You must write a last name"
            termino = false
        }
        else
            lastNameError.value=null

        if(idNumber.value.isNullOrEmpty())
        {
            idNumberError.value="You must write a id Number"
            termino = false
        }
        else{
            if(idNumber.value.toString().length <6){
                idNumberError.value = "Id must be greater than 6 characters"
                termino = false
            }
            else
                idNumberError.value = null
        }
        if(age.value.isNullOrEmpty())
        {
            ageError.value="You must write a age"
            termino = false
        }
        else{
            if(age.value.toString().toInt() >100){
                ageError.value = "Enter a valid age"
                termino = false
            }
            else
                ageError.value = null
        }
        if(urlPhoto.value.isNullOrEmpty())
        {
            Log.d("GoogleD", "Url photo es nulo ? ${urlPhoto.value}")
            urlPhoto.value = "N"
        }
        if( male.value!!){
            gender = "Male"
        }
        else if( female.value!!)
        {
            gender = "Female"
        }
        else if( other.value!!)
        {
            gender = "Other"
        }
        if (termino){
            singUpListener?.onStarted()
            Log.d("Google", "Entré en el termino")
            viewModelScope.launch {
                Log.d("Google", "Entré en el viewModelScope.Launch")
                val data =
                    repository.register(
                        email2.value!!,
                        password2.value!!,
                        nameSingUp.value.toString(),
                        lastName.value.toString(),
                        idNumber.value.toString().toInt(),
                        age.value.toString().toInt(),
                        gender,
                        singUpListener!!,
                        urlPhoto.value.toString()
                    )
                if(data){
                    Log.d("Google", "Data es ${data}")
                    singUpListener!!.onSuccess()
                }
                else
                    Log.d("Google", "Data es ${data}")
            }


        }
    }

    fun goToSignup() {
        authListener?.goToSingUp()
    }
    fun goToSignupWithGoogle() {
        authListener?.goToSingUpWithGoogle()

    }
    fun succesSignUp()
    {
        successSingUp.value = true
    }

    fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        viewModelScope.launch {
            val data =repository.signInWithCredential(credential,authListener)
            Log.d("GoogeD", "La data es ${data}")
            if( data!= null)
                authListener!!.onSuccess()
        }

    }

    fun isInDataBase(email: String?) {
//        repository.getUserByEmail(email, authListener) // Antiguo
        viewModelScope.launch {
            val data =repository.getUserByEmail(email, authListener)
            if (data != null) {
                Log.d("GoogeD", "Data es ${data.id}")
            }
            if(data!=null){
                authListener?.singUpWithGoogle()
            }
            else{
                authListener?.goToSingUpWithParameters()
            }
        }
    }

    fun startWithParameters(pname: String, plastName: String?, pemail: String?, purlPhoto: String?) {
        nameSingUp.value = pname
        lastName.value= plastName
        email2.value = pemail
        urlPhoto.value=purlPhoto
    }

}