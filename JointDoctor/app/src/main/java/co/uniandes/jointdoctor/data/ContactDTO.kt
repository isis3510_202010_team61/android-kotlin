package co.uniandes.jointdoctor.data

import android.graphics.Bitmap

class ContactDTO(name:String, number:String, image:String){
    var name = name
    var number = number
    var image = image
}