package co.uniandes.jointdoctor.Model

import android.util.Log
import co.uniandes.jointdoctor.View.auth.AuthListener
import co.uniandes.jointdoctor.View.auth.SingUpListener
import co.uniandes.jointdoctor.View.contact.ContactListener
import co.uniandes.jointdoctor.View.user.UserListener
import co.uniandes.jointdoctor.data.ContactDTO
import co.uniandes.jointdoctor.data.firebase.FirebaseSource
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.firestore.DocumentSnapshot

class User(private val firebase: FirebaseSource){

    suspend fun login(email: String, password: String, authListener: AuthListener?): AuthResult? {
        return firebase.login(email, password,authListener)
    }

    suspend fun register(
        email: String,
        password: String,
        name: String,
        lastName: String,
        idNumber: Int,
        age: Int,
        gender: String,
        singUpListener: SingUpListener,
        urlPhoto: String

    ): Boolean {
        return firebase.register(email, password,
            name,
            lastName,
            idNumber,
            age,
            gender,
            singUpListener,
            urlPhoto)
    }

    fun currentUser() = firebase.currentUser()

    fun getUser(userListener: UserListener?) = firebase.getUser(userListener)
    fun logout() = firebase.logout()
    suspend fun signInWithCredential(credential: AuthCredential, authListener: AuthListener?): AuthResult?{
        Log.d("GoogeD", "En el user despues de ir al repository")
        return firebase.singInCredentialGoogle(credential,authListener)
    }
//    suspend fun signInWithCredential(credential: AuthCredential, authListener: AuthListener?){
//        Log.d("GoogeD", "En el user despues de ir al repository")
//        firebase.singInCredentialGoogle(credential,authListener)
//    }

    suspend fun getUserByEmail(email: String?, authListener: AuthListener?): DocumentSnapshot? {
        return firebase.getUserByEmail(email!!,authListener)
    }

    suspend fun addContact(email: String?, value: ContactDTO?): Boolean? {
        if (value != null) {
            if (email != null) {
                return firebase.addContact(email,value)
            }
        }
        return false
    }
    suspend fun addContacts(email: String?, value: MutableList<ContactDTO>?): Boolean? {
        if (value != null) {
            if (email != null) {
                return firebase.addContacts(email,value)
            }
        }
        return false
    }
    suspend fun deleteContact(email: String?, value: ContactDTO?): Boolean? {
        if (value != null) {
            if (email != null) {
                return firebase.deleteContact(email,value)
            }
        }
        return false
    }
    fun getContacts(email: String?, listener: ContactListener) {
        if (email != null) {
            firebase.getContacts(email, listener)
        }
    }
}