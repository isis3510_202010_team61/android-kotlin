package co.uniandes.jointdoctor.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.Model.User

class ContactsViewModelFactory (private val repository: User): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ContactsViewModel(repository) as T
    }
}