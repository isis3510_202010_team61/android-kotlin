package co.uniandes.jointdoctor.View.auth

interface AuthListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
    fun goToSingUpWithGoogle()
    fun goToSingUp()
    fun goToSingUpWithParameters()
    fun singUpWithGoogle()

}