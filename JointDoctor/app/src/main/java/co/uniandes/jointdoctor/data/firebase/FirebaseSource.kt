package co.uniandes.jointdoctor.data.firebase

import android.util.Log
import co.uniandes.jointdoctor.View.auth.AuthListener
import co.uniandes.jointdoctor.View.auth.SingUpListener
import co.uniandes.jointdoctor.View.contact.ContactListener
import co.uniandes.jointdoctor.View.user.UserListener
import co.uniandes.jointdoctor.data.ContactDTO
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await

class FirebaseSource {

    private val firebaseAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }
    private val firebaseFirestore: FirebaseFirestore by lazy {
        FirebaseFirestore.getInstance()
    }

    suspend fun login(email: String, password: String, authListener: AuthListener?): AuthResult? {
        return try {
            val data = firebaseAuth.signInWithEmailAndPassword(email,password)
                .await()
            return data
        }
        catch (e:Exception){
            authListener?.onFailure(e.toString())
            null
        }
    }

    suspend fun register(
        email: String,
        password: String,
        name: String,
        lastName: String,
        idNumber: Int,
        age: Int,
        gender: String,
        singUpListener: SingUpListener,
        urlPhoto: String

    ): Boolean {
        return try{
            Log.d("Google", "Entré en el Register antes de createUserWithEmailAndPassword")
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                .await()
            Log.d("Google", "Entré en el Register antes de Collection y después de createUSer...")
            val user = hashMapOf(
                    "first" to name,
                    "last" to lastName,
                    "idNumber" to idNumber,
                    "age" to age,
                    "email" to email,
                    "gender" to gender,
                    "urlPhoto" to urlPhoto
                )
            firebaseFirestore.collection("users")
                .document(email)
                .collection("userInfo")
                .document()
                .set(user)
                .await()
            return true
        }
        catch (e:Exception){
            singUpListener.onFailure(e.toString())
            Log.d("Google", "Hay Exception... ${e.stackTrace} ${e.toString()}")
            return false
        }

    }
    fun getUser(userListener: UserListener?) {
        firebaseFirestore.collection("users")
            .document(currentUser()?.email.toString())
            .collection("userInfo")
            .get()
            .addOnSuccessListener { user ->
                userListener?.onSuccess(
                    user.documents[0]["first"] as String,
                    user.documents[0]["last"] as String,
                    (user.documents[0]["idNumber"] as Long).toInt(),
                    (user.documents[0]["age"] as Long).toInt(),
                    user.documents[0]["email"] as String,
                    user.documents[0]["gender"] as String,
                    user.documents[0]["urlPhoto"] as String
                )

                Log.d("usuario", user.documents[0]["email"] as String)
            }
    }

    suspend fun getUserByEmail(email: String, authListener: AuthListener?): DocumentSnapshot?
    {
        return try {
            val data =firebaseFirestore.collection("users")
                .document(email)
                .get()
                .await()
            return data
        }
        catch (e:Exception){
            Log.d("Google", "Hay Exception... ${e.stackTrace} ${e.toString()}")
            return null
        }
    }

    fun logout() = firebaseAuth.signOut()

    fun currentUser() = firebaseAuth.currentUser
    suspend fun singInCredentialGoogle(credential: AuthCredential, authListener: AuthListener?):AuthResult? {
        Log.d("GoogeD", "En el FireBaseSource")
        return try {
            val data = firebaseAuth.signInWithCredential(credential)
                .await()
            return  data
        }
        catch(e : Exception){
            e.message?.let { Log.e("GoogeD", it) }
            return null
        }
    }

    suspend fun addContacts(email: String, contactsDTOs: MutableList<ContactDTO>?): Boolean {
        Log.d("GoogeD", "Entré a addContacts")
        val aEliminar =firebaseFirestore
            .collection("users")
            .document(email)
            .collection("emergencyContacts")
            .get()
            .await()
        var batch = firebaseFirestore.batch()
        aEliminar.documents.forEach { document->
            Log.d("GoogeD", "Entré a borrar")
            batch.delete(document.reference)
        }
        if (contactsDTOs != null) {
            contactsDTOs.forEach { contactDTO->
                Log.d("Holas1234",contactDTO.name)
                val contact = hashMapOf(
                    "owner" to email,
                    "number" to contactDTO.number,
                    "name" to contactDTO.name,
                    "image" to contactDTO.image
                )
                var docRef =firebaseFirestore.collection("users")
                    .document(email)
                    .collection("emergencyContacts")
                    .document()
                batch.set(docRef,contact)
            }
                return try {
                    batch
                        .commit()
                        .await()
                    return true
                }
                catch (e:Exception){
                    e.message?.let { Log.e("GoogeD", it) }
                    return false
                }
            }

        return false
//        firebaseFirestore.collection("contacts")
//            .add(contact)

    }
    suspend fun addContact(email: String, contactDTO: ContactDTO): Boolean {
        val contact = hashMapOf(
            "owner" to email,
            "number" to contactDTO.number,
            "name" to contactDTO.name,
            "image" to contactDTO.image
        )
        return try {
            firebaseFirestore.collection("users")
                .document(email)
                .collection("emergencyContacts")
                .add(contact)
                .await()
            return true
        }
        catch (e:Exception){
            return false
        }
//        firebaseFirestore.collection("contacts")
//            .add(contact)

    }

    suspend fun deleteContact(email: String, contactDTO: ContactDTO): Boolean {
        return try {
            val contactos = firebaseFirestore.collection("users")
                .document(email)
                .collection("emergencyContacts")
                .whereEqualTo("name", contactDTO.name)
                .get()
                .await()
            contactos.documents.forEach {
                    contact->
                if(contact["number"] as String == contactDTO.number && contact["name"] as String == contactDTO.name){
                    Log.d("DeleteContact", "El id es ${contact.id}")
                    val result = firebaseFirestore.collection("users")
                        .document(email)
                        .collection("emergencyContacts")
                        .document(contact.id)
                        .delete()
                        .await()
                    return true
                }
            }
            return false
        }
        catch (e:Exception){
            return false
        }
    }
    fun getContacts(email: String, listener: ContactListener)
    {
        firebaseFirestore.collection("contacts")
            .whereEqualTo("owner", email)
            .get()
            .addOnSuccessListener{contacts ->
                val contactsList:MutableList<ContactDTO> = mutableListOf()
                Log.d("FirebaseNullContacts",contacts.documents.size.toString())
                if(contacts.documents.size==0)
                {
                    listener.failOrNullContacts()
                }
                else{
                    contacts.documents.forEach {
                            contact ->
                        val actual = ContactDTO(
                            contact["name"] as String,
                            contact["number"] as String,
                            contact["image"] as String
                        )
                        contactsList.add(actual)
                    }
                    listener.update(contactsList)
                }

            }
            .addOnFailureListener {
                listener.failOrNullContacts()
                Log.d("FirebaseNullContacts","Entré al addOnFail")
            }
    }

}