package co.uniandes.jointdoctor.View.home

interface HomeListener {
    fun onPermission()
    fun changeColor()
    fun openIntent(string: String)
}