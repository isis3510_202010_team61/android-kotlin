package co.uniandes.jointdoctor.`interface`

interface LoginResultCallBacks {
    fun onSuccess (message:String)
    fun onError (message:String)
}