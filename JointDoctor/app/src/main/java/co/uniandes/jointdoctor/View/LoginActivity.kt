package co.uniandes.jointdoctor.View

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.View.auth.AuthListener
import co.uniandes.jointdoctor.databinding.ActivityLoginBinding
import co.uniandes.jointdoctor.util.startHomeActivity
import co.uniandes.jointdoctor.viewmodel.AuthViewModel
import co.uniandes.jointdoctor.viewmodel.AuthViewModelFactory
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity() : AppCompatActivity(), AuthListener, KodeinAware {
    private var account: GoogleSignInAccount? = null
    val SING_UP_SUCCESS = 1
    val RC_SIGN_IN = 2
    override val kodein by kodein()


    private val factory: AuthViewModelFactory by instance()
    lateinit var mGoogleSignInClient : GoogleSignInClient
    private lateinit var authViewModel: AuthViewModel
    private lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient.signOut()
        authViewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.lifecycleOwner = this
        binding.authViewModel= authViewModel
        authViewModel.authListener = this

    }

    override fun onStarted() {
        progressbar.visibility = View.VISIBLE
    }

    override fun onSuccess() {

        progressbar.visibility = View.GONE
        startHomeActivity()
    }

    override fun onFailure(message: String) {
        progressbar.visibility = View.GONE
        Toast.makeText(
            this,
            " Registration failed due to: ${message.substring(message.indexOf(':'))}",
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun goToSingUpWithGoogle() {
        Log.d("GoogeD", "Entré al SingUp")
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun goToSingUp() {
        val intent = Intent(this, SignupActivity::class.java)
        startActivityForResult(intent, 1)
    }

    override fun goToSingUpWithParameters() {
        progressbar.visibility = View.GONE
        val intent = Intent(this, SignupActivity::class.java)
        Log.d("GoogleD", "Los valores son:" +
                "${account!!.email}" +
                "${account!!.displayName}" +
                "${account!!.familyName}" +
                "${account!!.photoUrl}" +
                "${account!!.givenName}")
        intent.putExtra("name", account!!.givenName)
        intent.putExtra("lastName", account!!.familyName)
        intent.putExtra("email", account!!.email)
        intent.putExtra("urlPhoto", account!!.photoUrl.toString())
        startActivityForResult(intent, 1)
    }

    override fun singUpWithGoogle() {
        Log.d("GoogeD", "El token es ${account}")
        authViewModel.firebaseAuthWithGoogle(account!!.idToken!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("GoogeD", "El código es ${requestCode}")
        if(resultCode == SING_UP_SUCCESS)
            authViewModel.succesSignUp()
        else if( requestCode == RC_SIGN_IN)
        {
            Log.d("GoogeD", "Entré al activity Result + data: ${data}")
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                account = task.getResult(ApiException::class.java)!!
                Log.d("GoogeD", "firebaseAuthWithGoogle:" + account!!.id +" "+account!!.email)
                onStarted()
                authViewModel.isInDataBase(account!!.email)

            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("GoogeD", "Google sign in failed", e)
                Log.w("GoogeD", "Google sign in failed ${e.cause}")
                // ...
            }
        }
    }



    override fun onStart() {
        super.onStart()
        authViewModel.user?.let {
            startHomeActivity()
        }
    }



}