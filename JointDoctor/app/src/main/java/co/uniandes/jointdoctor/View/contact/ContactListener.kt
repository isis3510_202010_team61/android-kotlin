package co.uniandes.jointdoctor.View.contact

import co.uniandes.jointdoctor.data.ContactDTO

interface ContactListener {
    fun openIntentPicker()
    fun callContact( contactDTO: ContactDTO)
    fun update(contactsList: MutableList<ContactDTO>)
    fun editContact(contactos: String)
    fun failOrNullContacts()
    fun writeChange(contactosOffline: String)
    fun deleteChange()
}