package co.uniandes.jointdoctor.View

import co.uniandes.jointdoctor.R
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.URLUtil
import android.widget.MediaController
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.databinding.ActivityExcercisesBinding
import co.uniandes.jointdoctor.util.ConnectionType
import co.uniandes.jointdoctor.util.NetworkMonitorUtil
import co.uniandes.jointdoctor.viewmodel.ExcercisesViewModel
import kotlinx.android.synthetic.main.activity_excercises.*


class ExcercisesActivity : AppCompatActivity() {

    private val networkMonitor = NetworkMonitorUtil(this)
    private val VIDEO_SAMPLE =
        "https://firebasestorage.googleapis.com/v0/b/jointdoctorapp.appspot.com/o/videoExerciseOne.mp4?alt=media&token=6f1dfdb5-1f8a-46d8-806d-e94ba98d71b4"
    // Current playback position (in milliseconds).
    private var mCurrentPosition = 0;
    // Tag for the instance state bundle.
    private val PLAYBACK_TIME = "play_time"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_excercises)
        networkMonitor.result = { isAvailable, type ->
            runOnUiThread {
                when (isAvailable) {
                    true -> {
                        when (type) {
                            ConnectionType.Wifi -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Wifi Connection")
                                showMessage(true)
                            }
                            ConnectionType.Cellular -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Cellular Connection")
                                showMessage(true)
                            }
                            else -> {
                            }
                        }
                    }
                    false -> {
                        Log.i("NETWORK_MONITOR_STATUS", "No Connection")
                        showMessage(false)
                    }
                }
            }
        }
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
        }
        val controller = MediaController(this)
        controller.setMediaPlayer(mVideoView)
        mVideoView.setMediaController(controller)

    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {
            val messageToUser = "You are offline now."
            textView7.visibility = View.VISIBLE
            textView7.text = messageToUser
        } else {
            textView7.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        networkMonitor.register()
    }

    override fun onStop() {
        super.onStop()
        // Media playback takes a lot of resources, so everything should be
        // stopped and released at this time.
        releasePlayer()
        networkMonitor.unregister()
    }

    override fun onStart() {
        super.onStart()
        // Load the media each time onStart() is called.
        initializePlayer()
    }

    override fun onPause() {
        super.onPause()
        // In Android versions less than N (7.0, API 24), onPause() is the
        // end of the visual lifecycle of the app.  Pausing the video here
        // prevents the sound from continuing to play even after the app
        // disappears.
        //
        // This is not a problem for more recent versions of Android because
        // onStop() is now the end of the visual lifecycle, and that is where
        // most of the app teardown should take place.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            mVideoView.pause()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Save the current playback position (in milliseconds) to the
        // instance state bundle.
        outState.putInt(PLAYBACK_TIME, mVideoView.currentPosition)
    }

    private fun initializePlayer() {
        Log.d("video", "Eentré al initializeplayer")
        // Show the "Buffering..." message while the video loads.
        mBufferingTextView.setVisibility(VideoView.VISIBLE)
        // Buffer and decode the video sample.
        val videoUri: Uri = getMedia(VIDEO_SAMPLE)
        mVideoView.setVideoURI(videoUri)

        // Listener for onPrepared() event (runs after the media is prepared).
        mVideoView.setOnPreparedListener { // Hide buffering message.
            Log.d("video", "Terminé el buffer")
            mBufferingTextView.setVisibility(VideoView.INVISIBLE)

            // Restore saved position, if available.
            if (mCurrentPosition > 0) {
                mVideoView.seekTo(mCurrentPosition)
            } else {
                // Skipping to 1 shows the first frame of the video.
                mVideoView.seekTo(1)
            }

            // Start playing!
            mVideoView.start()
        }

        // Listener for onCompletion() event (runs after media has finished
        // playing).
        mVideoView.setOnCompletionListener {
            Toast.makeText(
                this@ExcercisesActivity,
                R.string.toast_message,
                Toast.LENGTH_SHORT
            ).show()

            // Return the video position to the start.
            mVideoView.seekTo(0)
        }
    }


    // Release all media-related resources. In a more complicated app this
    // might involve unregistering listeners or releasing audio focus.
    private fun releasePlayer() {
        mVideoView.stopPlayback()
    }

    // Get a Uri for the media sample regardless of whether that sample is
    // embedded in the app resources or available on the internet.
    private fun getMedia(mediaName: String): Uri {
        return if (URLUtil.isValidUrl(mediaName)) {
            // Media name is an external URL.
            Log.d("video", "Es un url válido")
            Uri.parse(mediaName)
        } else {
            Log.d("video", "No es un url válido")
            // you can also put a video file in raw package and get file from there as shown below
            Uri.parse(
                "android.resource://" + packageName +
                        "/raw/" + mediaName
            )
        }
    }
}