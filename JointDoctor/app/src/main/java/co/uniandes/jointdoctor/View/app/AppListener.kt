package co.uniandes.jointdoctor.View.app

interface AppListener {
    fun onChange(oldFragment: String, currentFragment: String)
    fun logOut()
}