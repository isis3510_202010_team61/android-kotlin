package co.uniandes.jointdoctor.viewmodel

import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.SpeechRecognizer.*
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.uniandes.jointdoctor.View.home.HomeListener

class HomeViewModel() : ViewModel() , RecognitionListener {
    var textH1 = MutableLiveData<String>()
    var abrirIntent = false
    var homeListener: HomeListener? = null
    data class ViewState(
        val spokenText: String,
        val isListening: Boolean,
        val error: String?
    )

    private var viewState: MutableLiveData<ViewState>? = null
    lateinit var speechRecognizer: SpeechRecognizer
    var permissionToRecordAudio = false

    private val recognizerIntent: Intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
        putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "Hola")
        putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true)
    }
    var isListening = false
        get() = viewState?.value?.isListening ?: false
    fun getText() :String
    {
        return textH1.value.toString();
    }
    fun updateUI(uiOutput : ViewState)
    {
        textH1.value = uiOutput.spokenText
    }
    fun getViewState(): LiveData<ViewState> {
        if (viewState == null) {
            viewState = MutableLiveData()
            viewState?.value = initViewState()
        }

        return viewState as MutableLiveData<ViewState>
    }

    private fun initViewState() = ViewState(spokenText = "", isListening = false, error = null)

    fun startListening() {
        speechRecognizer.startListening(recognizerIntent)
        notifyListening(isRecording = true)

    }

    fun stopListening() {
        speechRecognizer.stopListening()
        notifyListening(isRecording = false)


    }

    private fun notifyListening(isRecording: Boolean) {
        viewState?.value = viewState?.value?.copy(isListening = isRecording)
        Log.i("logTag", "Terminé de escuchar")
        val text = viewState?.value?.spokenText
        Log.i("logTag", "Text stop ${text}")
        var intent = ""
        if (text != null) {
            if(text.contains("calendar")){
                intent = "calendar"
                viewState?.value = viewState?.value?.copy(spokenText = "")
                stopListening()
            } else if(text.contains("contact") ){
                intent = "contact"
                viewState?.value = viewState?.value?.copy(spokenText = "")
                stopListening()
            } else if( text.contains("exercise")) {
                intent = "exercise"
                viewState?.value = viewState?.value?.copy(spokenText = "")
                stopListening()
            }
        }
        homeListener!!.openIntent(intent)
    }
    fun goToCalendar()
    {
        val intent = "calendar"
        homeListener!!.openIntent(intent)
    }
    fun goToContact()
    {
        val intent = "contact"
        homeListener!!.openIntent(intent)
    }
    fun goToExercise()
    {
        val intent = "exercise"
        homeListener!!.openIntent(intent)
    }

    private fun updateResults(speechBundle: Bundle?) {
        val userSaid = speechBundle?.getStringArrayList(RESULTS_RECOGNITION)
        viewState?.value = viewState?.value?.copy(spokenText = userSaid?.get(0) ?: "")

    }


    fun onClick(){

        if (!permissionToRecordAudio) {
            homeListener?.onPermission()
        }
        if (isListening) {
            stopListening()
        } else {
            startListening()
        }

    }

    override fun onPartialResults(results: Bundle?) = updateResults(speechBundle = results)
    override fun onResults(results: Bundle?) = updateResults(speechBundle = results)
    override fun onEndOfSpeech() = notifyListening(isRecording = false)

    override fun onError(errorCode: Int) {
        viewState?.value = viewState?.value?.copy(error = when (errorCode) {
            ERROR_AUDIO -> "error_audio_error"
            ERROR_CLIENT -> "error_client"
            ERROR_INSUFFICIENT_PERMISSIONS -> "error_permission"
            ERROR_NETWORK -> "error_network"
            ERROR_NETWORK_TIMEOUT -> "error_timeout"
            ERROR_NO_MATCH -> "error_no_match"
            ERROR_RECOGNIZER_BUSY -> "error_busy"
            ERROR_SERVER -> "error_server"
            ERROR_SPEECH_TIMEOUT -> "error_timeout"
            else -> "error_unknown"
        })
    }

    override fun onReadyForSpeech(p0: Bundle?) {}
    override fun onRmsChanged(p0: Float) {}
    override fun onBufferReceived(p0: ByteArray?) {}
    override fun onEvent(p0: Int, p1: Bundle?) {}
    override fun onBeginningOfSpeech() {}
}