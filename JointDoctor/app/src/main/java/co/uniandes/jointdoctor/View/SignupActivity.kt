package co.uniandes.jointdoctor.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.View.auth.AuthListener
import co.uniandes.jointdoctor.View.auth.SingUpListener
import co.uniandes.jointdoctor.databinding.ActivitySignupBinding
import co.uniandes.jointdoctor.util.startHomeActivity
import co.uniandes.jointdoctor.viewmodel.AuthViewModel
import co.uniandes.jointdoctor.viewmodel.AuthViewModelFactory
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SignupActivity : AppCompatActivity(), SingUpListener, KodeinAware {
    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance()

    private lateinit var authViewModel: AuthViewModel
    private lateinit var binding: ActivitySignupBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        authViewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        binding.lifecycleOwner = this
        binding.authViewModel= authViewModel
        authViewModel.singUpListener = this
        var name = intent.getStringExtra("name")
        if(!name.isNullOrEmpty())
        {
            var lastName = intent.getStringExtra("lastName")
            var email = intent.getStringExtra("email")
            var  pUrlPhoto = intent.getStringExtra("urlPhoto")
            var urlPhoto = intent.extras.toString()
            Log.d("GoogleD", "El pUrlPhoto es ${urlPhoto}")
            authViewModel.startWithParameters(name, lastName, email, pUrlPhoto)
        }
    }

    override fun onStarted() {
        progressbar2.visibility = View.VISIBLE
    }

    override fun onSuccess() {
        progressbar2.visibility = View.GONE
        setResult(1)
        finish()
    }

    override fun onFailure(message: String) {
        progressbar2.visibility = View.GONE
        Toast.makeText(this, "La autenticación fue un fracaso por: ${message.substring(message.indexOf(':'))}", Toast.LENGTH_SHORT).show()

    }
}