package co.uniandes.jointdoctor.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.uniandes.jointdoctor.Model.User
import co.uniandes.jointdoctor.View.user.UserListener

class UserViewModel(private val repository: User) : ViewModel() {

    val user by lazy {
        repository.currentUser()
    }
    var name = MutableLiveData<String>("Loading...")
    val lastName = MutableLiveData<String>("Loading...")
    val idNumber = MutableLiveData<String>("Loading...")
    val age = MutableLiveData<String>("Loading...")
    val gender = MutableLiveData<String>("Loading...")
    val email = MutableLiveData<String>("Loading...")
    val imageUrl = MutableLiveData<String>("https://firebasestorage.googleapis.com/v0/b/jointdoctorapp.appspot.com/o/usuario.png?alt=media&token=e24b2524-d00f-499a-944a-1fdd672554bc")
    var userListener: UserListener? = null

    fun update(){
        userListener?.onStarted()
        repository.getUser(userListener)
    }

    fun update2(
        pName: String,
        pLastName: String,
        pIdNumber: Int,
        pAge: Int,
        pEmail: String,
        pGender: String,
        pUrlPhoto: String
    ) {
        name.value= pName
        lastName.value = pLastName
        idNumber.value= pIdNumber.toString()
        age.value = pAge.toString()
        email.value = pEmail
        gender.value = pGender
        if(pUrlPhoto != "null")
            imageUrl.value = pUrlPhoto
    }
}