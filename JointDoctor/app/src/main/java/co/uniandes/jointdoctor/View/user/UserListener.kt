package co.uniandes.jointdoctor.View.user

interface UserListener {
    fun onStarted()
    fun onSuccess(
        name: String,
        lastName: String,
        idNumber: Int,
        age: Int,
        gender: String,
        email: String,
        urlPhoto: String
    )
}