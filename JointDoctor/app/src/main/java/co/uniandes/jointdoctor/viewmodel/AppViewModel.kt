package co.uniandes.jointdoctor.viewmodel

import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.ViewModel
import co.uniandes.jointdoctor.View.app.AppListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.widget.Toolbar
import co.uniandes.jointdoctor.Model.User


class AppViewModel (private val repository: User): ViewModel(), BottomNavigationView.OnNavigationItemSelectedListener, Toolbar.OnMenuItemClickListener{
    var appListener: AppListener? = null
    var currentFragment:String = ""

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var oldFragment:String = currentFragment
        if(item.toString().equals("Buy")){
            currentFragment ="buy_frag"
        }
        else if(item.toString().equals("Home"))
            currentFragment = "home_frag"
        else
            currentFragment = "user_frag"
        Log.d("Fragmenas", "AppViewModel: Current= ${currentFragment} Old= ${oldFragment} ")
        appListener!!.onChange(oldFragment, currentFragment)
        item.isChecked=true
        return false
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        Log.d("Toolbar", "El item es ${item.toString()}")
        repository.logout()
        appListener!!.logOut()
        return true
    }
}