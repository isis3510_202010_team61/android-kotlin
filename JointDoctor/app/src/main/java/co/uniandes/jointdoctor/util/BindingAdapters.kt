package co.uniandes.jointdoctor.util

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.widget.Adapter
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.data.ContactDTO
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomnavigation.BottomNavigationView


class BindingAdapters {
    companion object{
        @JvmStatic
        @BindingAdapter("onNavigationItemSelected")
        fun setOnNavigationItemSelectedListener(
            view: BottomNavigationView,
            listener: BottomNavigationView.OnNavigationItemSelectedListener?
        ) {
            view.setOnNavigationItemSelectedListener(listener)
        }
        @JvmStatic
        @BindingAdapter("onNavigationItemSelected")
        fun onNavigationClicked(toolbar: Toolbar, listener: Toolbar.OnMenuItemClickListener?){
            toolbar.setOnMenuItemClickListener(listener)
        }
        @JvmStatic
        @BindingAdapter("profileImage")
        fun loadImage(view: ImageView, imageUrl: String?) {
            Glide.with(view.getContext())
                .load(imageUrl).apply(RequestOptions().circleCrop())
                .into(view)
        }
        @JvmStatic
        @BindingAdapter("adapter", "data")
        fun bind (recyclerView: RecyclerView, contactAdapter: ContactAdapter, data:MutableList<ContactDTO>) {
            Log.d("Contacts2", "Hola está es la posicion")
            recyclerView.adapter = contactAdapter
            Log.d("Contacts2", "Este es el adapter $contactAdapter")
            contactAdapter.updateData(data)
        }
        @JvmStatic
        @BindingAdapter("src")
        fun srcImage( view: ImageView, imageUrl: String) {
            if (imageUrl == "")
            {
                Glide.with(view.context)
                    .load(R.drawable.ic_account_box_black_18dp)
                    .apply(RequestOptions().circleCrop())
                    .into(view);
//                view.setImageDrawable(ContextCompat.getDrawable(view.context,
//                    R.drawable.ic_account_box_black_18dp))
            }
            else{
                Glide.with(view.context)
                    .load(Uri.parse(imageUrl))
                    .apply(RequestOptions().circleCrop())
                    .into(view);
            }
        }
    }

}