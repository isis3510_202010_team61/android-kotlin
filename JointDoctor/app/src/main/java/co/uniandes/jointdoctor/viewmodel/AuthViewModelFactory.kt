package co.uniandes.jointdoctor.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.Model.User

class AuthViewModelFactory (private val repository: User) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AuthViewModel(repository) as T
        }
}