package co.uniandes.jointdoctor

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.uniandes.jointdoctor.View.contact.ContactListener
import co.uniandes.jointdoctor.data.ContactDTO
import co.uniandes.jointdoctor.databinding.ActivityContactsBinding
import co.uniandes.jointdoctor.util.ConnectionType
import co.uniandes.jointdoctor.util.NetworkMonitorUtil
import co.uniandes.jointdoctor.viewmodel.ContactsViewModel
import co.uniandes.jointdoctor.viewmodel.ContactsViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_contacts.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.lang.reflect.Type


class ContactsActivity : AppCompatActivity(), KodeinAware,ContactListener {

    val PICK_CONTACT = 1
    val CONTACTS = "contacts"
    val CHANGES = "changes"
    val CONTACTSPREFERENCES = "contacts"

    override val kodein by kodein()
    private val networkMonitor = NetworkMonitorUtil(this)
    private lateinit var contactsViewModel: ContactsViewModel
    private lateinit var binding: ActivityContactsBinding
    private val factory: ContactsViewModelFactory by instance()
    private lateinit var preferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        preferences= getSharedPreferences(CONTACTSPREFERENCES, Context.MODE_PRIVATE)
        contactsViewModel = ViewModelProvider(
            this,
            factory
        ).get(ContactsViewModel::class.java)
        contactsViewModel.listener = this
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contacts)
        binding.lifecycleOwner = this
        binding.viewModel = contactsViewModel
        if (!(ContextCompat.checkSelfPermission(
                this.applicationContext!!,
                Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 1)
        } else {

        }
        checkIfAreChange()
        contact_list.apply {
            layoutManager = LinearLayoutManager(
                this@ContactsActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            setHasFixedSize(true)
        }
        networkMonitor.result = { isAvailable, type ->
            runOnUiThread {
                when (isAvailable) {
                    true -> {
                        when (type) {
                            ConnectionType.Wifi -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Wifi Connection")
                                showMessage(true)
                            }
                            ConnectionType.Cellular -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Cellular Connection")
                                showMessage(true)
                            }
                            else -> {
                            }
                        }
                    }
                    false -> {
                        Log.i("NETWORK_MONITOR_STATUS", "No Connection")
                        showMessage(false)
                    }
                }
            }
        }
        addSwipe()

    }

    fun addSwipe(){
        val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            private val background = ColorDrawable(Color.RED)
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                contactsViewModel.delete( position)
//                if (direction == ItemTouchHelper.LEFT) {
//                    adapter!!.removeItem(position)
//                } else {
//                    removeView()
//                    edit_position = position
//                    alertDialog!!.setTitle("Edit Name")
//                    et_name!!.setText(names[position])
//                    alertDialog!!.show()
//                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                val itemView = viewHolder.itemView
                val backgroundCornerOffset = 20

                if (dX > 0) { // Swiping to the right
                    background.setBounds(
                        itemView.left, itemView.top,
                        itemView.left + dX.toInt() + backgroundCornerOffset,
                        itemView.bottom
                    )
                } else if (dX < 0) { // Swiping to the left
                    background.setBounds(
                        itemView.right + dX.toInt() - backgroundCornerOffset,
                        itemView.top, itemView.right, itemView.bottom
                    )
                } else { // view is unSwiped
                    background.setBounds(0, 0, 0, 0)
                }
                background.draw(c)
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(contact_list)
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {

        }

    }


    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {
            val messageToUser = "You are offline now."
            textView6.visibility = View.VISIBLE
            textView6.text = messageToUser
            contactsViewModel.offline()
        } else {
            textView6.visibility = View.GONE
            contactsViewModel.online()
            checkIfAreChange()
        }
    }

    override fun onResume() {
        super.onResume()
        networkMonitor.register()
        val stringContacts = preferences.getString(CONTACTS, null)
        if(stringContacts==null){
            contactsViewModel.searhForContacts()
        }
        else{
            val gson = Gson()
            val type: Type = object : TypeToken<MutableList<ContactDTO>?>() {}.getType()
            val contacts = gson.fromJson<MutableList<ContactDTO>>(stringContacts, type)
            contactsViewModel.update(contacts)

        }
    }
    fun checkIfAreChange()
    {
        if(contactsViewModel.hayCambios.value!!){
            val stringContactsChanges = preferences.getString(CONTACTS, null)
            if(stringContactsChanges!=null)
            {
                Log.d("Holas1234",stringContactsChanges)
                val gson = Gson()
                val type: Type = object : TypeToken<MutableList<ContactDTO>?>() {}.getType()
                val contacts = gson.fromJson<MutableList<ContactDTO>>(stringContactsChanges, type)
                Log.d("Holas1234",contacts.toString())
                contactsViewModel.updateOfflineContacts(contacts)
            }
        }

    }
    override fun onStop() {
        super.onStop()
        networkMonitor.unregister()
    }

    override fun openIntentPicker() {
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        startActivityForResult(intent, PICK_CONTACT)
    }

    override fun callContact(contactDTO: ContactDTO) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.setData(Uri.parse("tel:" + contactDTO.number))
        Log.d("ContactoClick", contactDTO.name)
        startActivity(intent)
    }

    override fun update(contactsList: MutableList<ContactDTO>) {
        contactsViewModel.update(contactsList)
    }

    override fun editContact(contactos: String) {
        textView3.visibility = View.GONE
        val editor = preferences.edit()
        editor.putString(CONTACTS, contactos)
        editor.commit()

    }

    override fun writeChange(contactosOffline: String)
    {
        val editor = preferences.edit()
        editor.putString(CHANGES, contactosOffline)
        editor.commit()
    }
    override fun deleteChange()
    {
        val editor = preferences.edit()
        editor.remove(CHANGES)
        editor.commit()
    }
    override fun failOrNullContacts() {
        textView3.visibility = View.VISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                val contactData = data?.data
                var c = contentResolver.query(contactData!!, null, null, null, null)
                if (c!!.moveToFirst()) {
                    val name =
                        c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val number =
                        c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    var photo_uri1 =
                        c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))
                    if(photo_uri1.isNullOrEmpty())
                        photo_uri1 = ""
                    contactsViewModel.addContact(name, number, photo_uri1)

                }
            }
        }
    }


}