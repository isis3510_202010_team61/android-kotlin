    package co.uniandes.jointdoctor.util

    import android.content.Context
    import android.util.Log
    import android.view.LayoutInflater
    import android.view.View
    import android.view.ViewGroup
    import android.widget.FrameLayout
    import androidx.databinding.DataBindingUtil
    import androidx.recyclerview.widget.RecyclerView
    import co.uniandes.jointdoctor.R
    import co.uniandes.jointdoctor.data.ContactDTO
    import co.uniandes.jointdoctor.databinding.ContactChildBinding
    import co.uniandes.jointdoctor.viewmodel.ContactsViewModel
    import kotlinx.android.synthetic.main.contact_child.view.*


    class ContactAdapter(items: MutableList<ContactDTO>, contactsViewModel: ContactsViewModel) : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>(){

        private var list:MutableList<ContactDTO> = items
        private var contactsViewModel = contactsViewModel

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ):ContactViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(
                R.layout.contact_child,
                parent,
                false
            )
            return ContactViewHolder(itemView)

        }

        override fun onBindViewHolder(contactViewHolder: ContactViewHolder, position: Int) {
            var contact = list.get(position)
            Log.d("Contacts2", "Hola está es la posicion${position} ${contact.name}")
            contactViewHolder.setContact(contact)
            contactViewHolder.setModelView(contactsViewModel)

//            if(list[position].image != null)
//                contactViewHolder.profile.setImageBitmap(list[position].image)
//            else
//                contactViewHolder.profile.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        context,
//                        R.drawable.ic_account_box_black_18dp
//                    )
//                )
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onViewAttachedToWindow(holder: ContactViewHolder) {
            super.onViewAttachedToWindow(holder)
            holder.bind()
        }

        override fun onViewDetachedFromWindow(holder: ContactViewHolder) {
            super.onViewDetachedFromWindow(holder)
            holder.unbind()
        }
        fun removeItem(position: Int) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }

        fun updateData(data: MutableList<ContactDTO>?) {
            if (data == null || data.isEmpty()) {
                list.clear()
            } else {
                list = data
                Log.d("Contacts", "Hola updateData ${list}")
            }
            notifyDataSetChanged()
        }

        class ContactViewHolder(v: View) : RecyclerView.ViewHolder(
            v
        ) {


            /* package */
            var binding: ContactChildBinding? = null
            /* package */
            fun bind() {
                if (binding == null) {

                    binding = DataBindingUtil.bind(itemView)

                }
            }

            /* package */
            fun unbind() {
                binding?.unbind()
            }

            /* package */
            fun setContact(contactDTO: ContactDTO) {
                if (binding != null) {
                    binding!!.contact =contactDTO
                }
            }
            fun setModelView(contactsViewModel: ContactsViewModel) {
                if (binding != null) {
                    binding!!.viewModel = contactsViewModel
                }
            }

            /* package */
            init {
                bind()
            }
        }


    }