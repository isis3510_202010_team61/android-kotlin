package co.uniandes.jointdoctor.View

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.ContactsActivity
import co.uniandes.jointdoctor.View.ExcercisesActivity
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.View.home.HomeListener
import co.uniandes.jointdoctor.databinding.HomeFragmentBinding
import co.uniandes.jointdoctor.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.home_fragment.*

class HomeFragment() : Fragment(), HomeListener {

    companion object {
        fun newInstance() = HomeFragment()
    }
    private val permission = 100
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var speech: SpeechRecognizer
    private lateinit var recognizerIntent: Intent
    private lateinit var binding : HomeFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel = ViewModelProvider(this).get(
            HomeViewModel::class.java
        )
        homeViewModel.speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity).apply {
            setRecognitionListener(homeViewModel)
        }
        homeViewModel.permissionToRecordAudio = checkAudioRecordingPermission()
        homeViewModel.homeListener = this
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        binding.homeViewModel = homeViewModel
        binding.lifecycleOwner = this
        homeViewModel.getViewState().observe(viewLifecycleOwner, Observer<HomeViewModel.ViewState> {
            homeViewModel.updateUI(it)
        })

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        speech = SpeechRecognizer.createSpeechRecognizer(activity)
        Log.i(
            "logTag", "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(
                activity
            )
        )


    }
    private fun checkAudioRecordingPermission() =
        ContextCompat.checkSelfPermission(activity?.applicationContext!!, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED

    override fun onPermission() {
        ActivityCompat.requestPermissions(activity as AppActivity, arrayOf(Manifest.permission.RECORD_AUDIO),1)
    }

    override fun changeColor() {
        if(micOn.visibility != View.VISIBLE){
            micOn.visibility = View.VISIBLE
            micOff.visibility = View.GONE
        }
        else{
            micOn.visibility = View.GONE
            micOff.visibility = View.VISIBLE
        }




    }

    override fun openIntent(string: String) {
        Log.i("logTag", "Text stop123123 ${string}")
        if(string.contains("calendar")){
            Intent(view?.context, CalendarActivity::class.java).also {
                view?.context?.startActivity(it)
            }
        } else if(string.contains("contact") ){

            Intent(view?.context, ContactsActivity::class.java).also {
                view?.context?.startActivity(it)
            }
        } else if( string.contains("exercise")) {
            Intent(view?.context, ExcercisesActivity::class.java).also {
                view?.context?.startActivity(it)
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 1) {
            homeViewModel.permissionToRecordAudio = grantResults[0] == PackageManager.PERMISSION_GRANTED
        }

        if (homeViewModel.permissionToRecordAudio) {
            homeViewModel.startListening()
        }
    }


}