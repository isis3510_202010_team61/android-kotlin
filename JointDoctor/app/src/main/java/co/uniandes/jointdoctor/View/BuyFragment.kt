package co.uniandes.jointdoctor.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.viewmodel.BuyViewModel
import co.uniandes.jointdoctor.R

class BuyFragment : Fragment() {

    companion object {
        fun newInstance() = BuyFragment()
    }

    private lateinit var viewModel: BuyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.buy_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BuyViewModel::class.java)

    }

}
