package co.uniandes.jointdoctor.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import co.uniandes.jointdoctor.R
import co.uniandes.jointdoctor.View.app.AppListener
import co.uniandes.jointdoctor.databinding.ActivityAppBinding
import co.uniandes.jointdoctor.util.startLoginActivity
import co.uniandes.jointdoctor.viewmodel.AppViewModel
import co.uniandes.jointdoctor.viewmodel.AppViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AppActivity : AppCompatActivity(), KodeinAware, AppListener {
    val FRAG_HOME = "home_frag"
    val FRAG_BUY = "buy_frag"
    val FRAG_USER = "user_frag"
    override val kodein by kodein()
    private lateinit var appViewModel: AppViewModel
    private val factory: AppViewModelFactory by instance()
    private lateinit var binding : ActivityAppBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        appViewModel = ViewModelProvider(this, factory).get(AppViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_app)
        binding.lifecycleOwner = this
        binding.appViewModel = appViewModel
        appViewModel.appListener=this
        appViewModel.currentFragment=FRAG_HOME
        onChange("", FRAG_HOME)

    }

    override fun onChange(oldFragment: String, currentFragment: String) {
        Log.d("Fragmenas", "${supportFragmentManager.fragments} viejo=${oldFragment}, nuevo ${currentFragment}")
        if(oldFragment == "")
        {
            var frag = HomeFragment.newInstance()
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.fragment_container, frag, currentFragment)
                    .show(frag)
                    .commit()
        }
        else{
            var oldfrag = supportFragmentManager.findFragmentByTag(oldFragment)
            var currentfrag = supportFragmentManager.findFragmentByTag(currentFragment)
            if (currentfrag == null && oldFragment != currentFragment) {
                Log.d("Fragmenas", "${currentFragment} not found, creating a new one.")
                when (currentFragment) {
                    FRAG_HOME -> {
                        currentfrag = HomeFragment.newInstance()
                    }
                    FRAG_BUY -> {
                        currentfrag = BuyFragment.newInstance()
                    }
                    FRAG_USER -> {
                        currentfrag = UserFragment.newInstance()
                    }
                }
                supportFragmentManager
                    .beginTransaction()
                    .hide(oldfrag!!)
                    .add(R.id.fragment_container, currentfrag!!, currentFragment)
                    .show(currentfrag)
                    .commit()
            } else {
                Log.d("Fragmenas", "$oldfrag found.")
                supportFragmentManager
                    .beginTransaction()
                    .hide(oldfrag!!)
                    .show(currentfrag!!)
                    .commit()
            }
        }
    }

    override fun logOut() {
        startLoginActivity()
    }


}