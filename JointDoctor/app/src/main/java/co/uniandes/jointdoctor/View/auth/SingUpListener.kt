package co.uniandes.jointdoctor.View.auth

interface SingUpListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
}