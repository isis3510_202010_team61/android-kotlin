package co.uniandes.jointdoctor.viewmodel

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.uniandes.jointdoctor.Model.User
import co.uniandes.jointdoctor.View.contact.ContactListener
import co.uniandes.jointdoctor.data.ContactDTO
import co.uniandes.jointdoctor.util.ContactAdapter
import com.google.gson.Gson
import kotlinx.coroutines.launch


class ContactsViewModel(private val repository: User): ViewModel()
{
    val contacts = MutableLiveData<MutableList<ContactDTO>>(mutableListOf())
    val offlinecontacts = MutableLiveData<MutableList<ContactDTO>>(mutableListOf())
    val buttonVisible = MutableLiveData<Boolean>(true)
    val conected = MutableLiveData<Boolean>(true)
    val hayCambios = MutableLiveData<Boolean>(false)
    val changes = MutableLiveData<Boolean>(false)
    val contactAdapter = MutableLiveData<ContactAdapter>(
        ContactAdapter(
            contacts.value!!,
            this
        )
    )
    lateinit var listener: ContactListener
    private lateinit var preferences: SharedPreferences

    fun add()
    {
        listener.openIntentPicker()
    }
    fun addContact(name: String, number: String, image: String)
    {
        val contacto = ContactDTO(name, number, image)
        contacts.value?.add(contacto)
        contactAdapter.value?.updateData(contacts.value)
        moreThanFive()
        val gson = Gson()
        listener.editContact(gson.toJson(contacts.value))
        Log.d("SharedPreferences", gson.toJson(contacts.value))
        addContactInDatabase(contacto)
    }
    fun addContactInDatabase(contacto: ContactDTO)
    {
        if(conected.value!!){
            viewModelScope.launch{
                val data = repository.addContact(repository.currentUser()?.email, contacto)
            }
        }
        else{
            hayCambios.value = true
        }
    }
    fun addContacstInDatabase(contacts: MutableList<ContactDTO>?)
    {
        if(conected.value!!){
            viewModelScope.launch{
                val data = repository.addContacts(repository.currentUser()?.email, contacts)
                hayCambios.value = false
            }
        }
        else{
            hayCambios.value = true
        }
    }
    fun callContact(contactDTO: ContactDTO)
    {
        listener.callContact(contactDTO)
    }

    fun searhForContacts() {
        if(conected.value!!)
            repository.getContacts(repository.currentUser()?.email, listener)
    }

    fun update(contactsList: MutableList<ContactDTO>) {
        Log.d("SharedPreferences", contactsList.toString())
        contacts.value = contactsList
        contactAdapter.value?.updateData(contacts.value)
        moreThanFive()
    }
    fun moreThanFive()
    {
        if(contacts.value?.size!! >= 5){
            buttonVisible.value=false
        }else{
            buttonVisible.value=true
        }
    }

    fun offline()
    {
        conected.value = false
    }

    fun online()
    {
        conected.value = true
    }

    fun updateOfflineContacts(contacts: MutableList<ContactDTO>?) {
        hayCambios.value = false
        addContacstInDatabase(contacts)
    }

    fun delete(position: Int) {
        Log.d("DeleteContact", "Entré al método delete")
        val contacto = contacts.value?.removeAt(position)
        Log.d("DeleteContact", "El contacto es ${contacto.toString()}")
        contactAdapter.value?.updateData(contacts.value)
        moreThanFive()
        val gson = Gson()
        listener.editContact(gson.toJson(contacts.value))
        Log.d("SharedPreferences", gson.toJson(contacts.value))
        if (contacto != null) {
            deleteContactInDatabase(contacto)
        }
    }

    private fun deleteContactInDatabase(contacto: ContactDTO) {
        if(conected.value!!){
            viewModelScope.launch{
                val data = repository.deleteContact(repository.currentUser()?.email, contacto)
                if(data!!){
                    Log.d("DeleteContact", "exito")
                }
                else{
                    Log.d("DeleteContact", "Fallo")
                }
            }
        }
        else{
            hayCambios.value = true
        }
    }


}