# Android Kotlin
A new Kotlin application.
# Getting Started
This project is a starting point for a Kotlin application.

A few resources to get you started if this is your first Flutter project:
- [Official documentation](https://developer.android.com/kotlin/get-started)
- [Kotlin Step-by-Step Tutorial for Beginners](https://www.hackster.io/tellmehow/kotlin-step-by-step-tutorial-for-beginners-c45bd7)

Steps to follow in order to execute the jointdoctor_app
1. Clone the project from the repository
2. Once the project is cloned Open it on Android Studio
3. Open the project and sync in order to instal the dependencies.
4. Finally click on "Run App" or Mayus + F10

